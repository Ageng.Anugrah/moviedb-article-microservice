package kelompokb4.moviedbarticlemicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MovieDbArticleMicroserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(MovieDbArticleMicroserviceApplication.class, args);
    }

}
