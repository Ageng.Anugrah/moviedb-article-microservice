package kelompokb4.moviedbarticlemicroservice.article.controller;

import kelompokb4.moviedbarticlemicroservice.article.core.Article;
import kelompokb4.moviedbarticlemicroservice.article.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/article")
public class ArticleController {
    @Autowired
    private ArticleService artikelService;

    @Transactional
    @GetMapping(path = "/{idArticle}", produces = {"application/json"})
    public ResponseEntity getArticleById(@PathVariable("idArticle") int idArticle) {
        Article article = artikelService.getArtikelById(idArticle);
        return article != null ? ResponseEntity.ok(article) : new ResponseEntity(HttpStatus.NOT_FOUND);
    }

    @GetMapping(path = "/", produces = {"application/json"})
    public ResponseEntity<Iterable<Article>> getListArticle() {
        return ResponseEntity.ok(artikelService.getListArtikel());
    }

    @PostMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity postMahasiswa(@RequestBody Article artikel) {
        return ResponseEntity.ok(artikelService.createArtikel(artikel));
    }
}
