package kelompokb4.moviedbarticlemicroservice.article.core;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;


@Entity
@Table(name = "article")
@Data
@NoArgsConstructor
public class Article {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int idArtikel;

    @Column(name = "username")
    private String username;

    @Column(name = "judul")
    private String judul;

    @Lob
    @Column(name = "isi")
    private String isi;

    @Column(name = "tanggal_post")
    private String tanggalPost;

    public Article (String judul, String isi, String username, String tanggalPost){
        this.judul = judul;
        this.isi = isi;
        this.username = username;
        this.tanggalPost = tanggalPost;
    }

}