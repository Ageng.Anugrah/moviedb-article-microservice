package kelompokb4.moviedbarticlemicroservice.article.repository;

import kelompokb4.moviedbarticlemicroservice.article.core.Article;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticleRepository extends JpaRepository<Article, Integer>{
    Article findByIdArtikel(int idArtikel);
}
