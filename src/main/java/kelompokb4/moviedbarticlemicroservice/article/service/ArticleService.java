package kelompokb4.moviedbarticlemicroservice.article.service;

import kelompokb4.moviedbarticlemicroservice.article.core.Article;

public interface ArticleService {
    Article createArtikel(Article artikel);
    Iterable<Article> getListArtikel();
    Article getArtikelById(int idArtikel);
}