package kelompokb4.moviedbarticlemicroservice.article.service;


import kelompokb4.moviedbarticlemicroservice.article.core.Article;
import kelompokb4.moviedbarticlemicroservice.article.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ArticleServiceImpl implements ArticleService {

    @Autowired
    private ArticleRepository artikelRepository;

    @Override
    public Article createArtikel(Article artikel) {
        artikelRepository.save(artikel);
        return artikel;
    }

    @Override
    public Iterable<Article> getListArtikel() {
        return artikelRepository.findAll();
    }

    @Override
    public Article getArtikelById(int idArtikel) {
        return artikelRepository.findByIdArtikel(idArtikel);
    }
}
