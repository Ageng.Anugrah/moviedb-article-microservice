package kelompokb4.moviedbarticlemicroservice.article.controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import kelompokb4.moviedbarticlemicroservice.article.core.Article;
import kelompokb4.moviedbarticlemicroservice.article.repository.ArticleRepository;
import kelompokb4.moviedbarticlemicroservice.article.service.ArticleService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import static org.mockito.Mockito.when;

import static org.mockito.Mockito.lenient;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(controllers = ArticleController.class)
public class ArticleControllerTest {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    private ArticleService articleService;

    @MockBean
    private ArticleRepository articleRepository;

    private Article article;

    @BeforeEach
    public void init() {
        article = new Article();
        article.setIdArtikel(1);
        article.setIsi("Isi");
        article.setJudul("Judul");
        article.setUsername("McDumbDumb");
        article.setTanggalPost("2021-20-21");

        lenient().when(articleService.getArtikelById(1)).thenReturn(article);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    public void testListAllArticle() throws Exception{
        mockMvc.perform(get("/article/")).andExpect(status().isOk());
    }

    @Test
    public void getArticleByIdTest() throws Exception{
        mockMvc.perform(get("/article/1/")).andExpect(status().isOk());
    }

    @Test
    public void postArticleTest() throws Exception{
        when(articleService.createArtikel(article)).thenReturn(article);
        mockMvc.perform(post("/article/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapToJson(article)))
                .andExpect(status().isOk());
    }
}
