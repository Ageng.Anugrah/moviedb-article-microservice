package kelompokb4.moviedbarticlemicroservice.article.service;

import kelompokb4.moviedbarticlemicroservice.article.core.Article;
import kelompokb4.moviedbarticlemicroservice.article.repository.ArticleRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class ArticleServiceTest {

    @Mock
    private ArticleRepository articleRepository;


    @InjectMocks
    private ArticleServiceImpl articleServiceImpl;

    Article article;

    @BeforeEach
    void init(){
        article = new Article("Judul","Isi","McDumbDumb","2021-20-21");
        article.setIdArtikel(1);
        ArrayList<Article> articles = new ArrayList<>();
        articles.add(article);

        lenient().when(articleRepository.findByIdArtikel(1)).thenReturn(article);
        lenient().when(articleRepository.findAll()).thenReturn(articles);

    }

    @Test
    public void testGetArticleById() {
        Article article = articleServiceImpl.getArtikelById(1);
        assertNotNull(article);
    }

    @Test
    public void getListArticle() {
        Iterable<Article> articles = articleServiceImpl.getListArtikel();
        assertNotNull(articles);
    }

    @Test
    public void createArticle() {
        lenient().when(articleServiceImpl.createArtikel(article)).thenReturn(article);
        Article resultArticle = articleServiceImpl.createArtikel(article);
        Assertions.assertEquals(resultArticle.getJudul(), article.getJudul());
    }

}